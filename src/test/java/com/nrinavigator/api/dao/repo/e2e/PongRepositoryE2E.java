package com.nrinavigator.api.dao.repo.e2e;

import com.nrinavigator.api.dao.repo.PongRepository;
import com.nrinavigator.api.dao.repo.domain.NewTable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PongRepositoryE2E {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        PongRepository pongRepository = applicationContext.getBean(PongRepository.class);
        NewTable newTable = new NewTable();
        newTable.setPong("pong");
        newTable.setPongPK(1);
        pongRepository.save(newTable);
        System.out.println("pongRepository "+ pongRepository.findOne(1));
    }
}
