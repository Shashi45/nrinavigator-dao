package com.nrinavigator.api.dao.repo.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

import static com.nrinavigator.api.dao.repo.common.validations.RegExConstants.ALPHA_WITH_SPACES;
import static com.nrinavigator.api.dao.repo.common.validations.RegExConstants.VALID_PHONE_CHARACTERS;
import static javax.persistence.InheritanceType.JOINED;

@SuppressWarnings("unused")
@Entity
@Inheritance(strategy = JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "guid")
public class Restaurant implements Serializable {

    @Id
    @GeneratedValue(generator = "guid")
    @GenericGenerator(name = "guid", strategy = "com.nrinavigator.api.dao.repo.common.generator.UseExistingOrGenerateUUIDGenerator")
    private String guid;
    @Pattern(regexp = ALPHA_WITH_SPACES)
    private String name;
    private Address address;
    private String url;
    @Column(name = "icon_url")
    private String iconUrl;
    private String vicinity;

    private Timings timings;

    private Cuisine cuisine;
    @Pattern(regexp = VALID_PHONE_CHARACTERS)
    @Column(name = "phone_number")
   private String phoneNumber;
    @Column(name = "global_address")
    private GlobalAddress globalAddress;

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(getClass().equals((obj.getClass())))) {
            return false;
        }

        Restaurant that = (Restaurant) obj;

        return this.guid.equals(that.guid);
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public Timings getTimings() {
        return timings;
    }

    public void setTimings(Timings timings) {
        this.timings = timings;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    public void setCuisine(Cuisine cuisine) {
        this.cuisine = cuisine;
    }

    public GlobalAddress getGlobalAddress() {
        return globalAddress;
    }

    public void setGlobalAddress(GlobalAddress globalAddress) {
        this.globalAddress = globalAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "guid='" + guid + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                ", url='" + url + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", vicinity='" + vicinity + '\'' +
                ", timings=" + timings +
                ", cuisine=" + cuisine +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", globalAddress=" + globalAddress +
                '}';
    }
}
