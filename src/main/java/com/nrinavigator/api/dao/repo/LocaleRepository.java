package com.nrinavigator.api.dao.repo;


import com.nrinavigator.api.dao.repo.domain.Locale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocaleRepository extends JpaRepository<Locale, Integer> {
}
