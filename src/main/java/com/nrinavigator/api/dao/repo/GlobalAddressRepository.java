package com.nrinavigator.api.dao.repo;


import com.nrinavigator.api.dao.repo.domain.GlobalAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalAddressRepository extends JpaRepository<GlobalAddress, Integer> {
}
