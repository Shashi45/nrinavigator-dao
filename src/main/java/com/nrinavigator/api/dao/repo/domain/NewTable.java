package com.nrinavigator.api.dao.repo.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "new_table")
public class NewTable implements Serializable{
    @Id
    @Column(name = "pong_PK")
    int pongPK;
    @Column(name = "pong")
    String pong;


    public int getPongPK() {
        return pongPK;
    }

    public void setPongPK(int pongPK) {
        this.pongPK = pongPK;
    }

    public String getPong() {
        return pong;
    }

    public void setPong(String pong) {
        this.pong = pong;
    }

    @Override
    public String toString() {
        return "NewTable{" +
                "pongPK=" + pongPK +
                ", pong='" + pong + '\'' +
                '}';
    }
}
