package com.nrinavigator.api.dao.repo.common.enums;

public enum Week {


    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday");


    String name;

    Week(String name) {
        this.name = name;
    }


}
