package com.nrinavigator.api.dao.repo;


import com.nrinavigator.api.dao.repo.domain.Cuisine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuisineRepository extends JpaRepository<Cuisine, Integer> {
}
