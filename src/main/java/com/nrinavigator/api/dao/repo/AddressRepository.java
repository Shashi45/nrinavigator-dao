package com.nrinavigator.api.dao.repo;


import com.nrinavigator.api.dao.repo.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, String> {
}
