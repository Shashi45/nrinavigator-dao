package com.nrinavigator.api.dao.repo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.FetchType.LAZY;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "guid")
public class Cuisine implements Serializable {
    @Id
    @GeneratedValue(generator = "guid")
    @GenericGenerator(name = "guid", strategy = "com.nrinavigator.api.dao.repo.common.generator.UseExistingOrGenerateUUIDGenerator")
    private String guid;
    @Column(name = "cuisine_type")
    private String cuisineType;
    private boolean bar;
    private boolean veg;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "RESTAURANT_GUID")
    @JsonBackReference
    private Restaurant restaurant;

    public Cuisine(String cuisineType, boolean bar, boolean veg) {
    }

    public Cuisine() {
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(String cuisineType) {
        this.cuisineType = cuisineType;
    }

    public boolean isBar() {
        return bar;
    }

    public void setBar(boolean bar) {
        this.bar = bar;
    }

    public boolean isVeg() {
        return veg;
    }

    public void setVeg(boolean veg) {
        this.veg = veg;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
