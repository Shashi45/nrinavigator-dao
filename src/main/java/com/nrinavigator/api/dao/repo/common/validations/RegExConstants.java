package com.nrinavigator.api.dao.repo.common.validations;

public class RegExConstants {

    public static final String ALPHA_WITH_SPACES = "^[a-zA-Z\\s]+$";
    public static final String VALID_PHONE_CHARACTERS = "^[0-9\\s\\(\\)\\-\\+\\#]*$";
    public static final String NUMERIC = "^[0-9]+$";
    public static final String FIVE_DIGIT_ZIPCODE = "^[0-9]{5}$";
    public static final String ALPHA_WITH_SPACES_OR_EMPTY = "^[a-zA-Z\\s]*$";
}
