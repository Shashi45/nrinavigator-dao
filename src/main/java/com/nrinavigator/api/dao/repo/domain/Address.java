package com.nrinavigator.api.dao.repo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nrinavigator.api.dao.repo.common.validations.RegExConstants;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.InheritanceType.JOINED;

@SuppressWarnings("unused")
@Entity
@Inheritance(strategy = JOINED)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "guid")
public class Address implements Serializable {
    @Id
    @GeneratedValue(generator = "guid")
    @GenericGenerator(name = "guid", strategy = "com.nrinavigator.api.dao.repo.common.generator.UseExistingOrGenerateUUIDGenerator")
    private String guid;
    @Column(name = "street_address")
    private String streetAddress;
    private String city;
    private String state;
    @Pattern(regexp = RegExConstants.FIVE_DIGIT_ZIPCODE)
    @Column(name = "zip_code")
    private String zipCode;

    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "RESTAURANT_GUID")
    @JsonBackReference
    private Restaurant restaurant;

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
}
