package com.nrinavigator.api.dao.repo;


import com.nrinavigator.api.dao.repo.domain.Timings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimingRepository extends JpaRepository<Timings, Integer>{
}
