package com.nrinavigator.api.dao.repo;

import com.nrinavigator.api.dao.repo.domain.NewTable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("pongRepository")
public interface PongRepository extends CrudRepository<NewTable, Integer> {

    NewTable findOne(Integer aLong);

    NewTable save(NewTable newTable);
}
